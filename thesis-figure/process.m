function data = process()

    % read data
    data = read_data('data.xlsx');
    
    % fit data
    fitfun = @fit_quadruplet_avg;
    data(1).wt  = fitfun(data(1).wt);
    data(1).mut = fitfun(data(1).mut);
    data(2).wt  = fitfun(data(2).wt);
    data(2).mut = fitfun(data(2).mut);

%     % show fits
%     showfun = @show_avg;
%     showfun( data(1).wt, 'WT1' );
%     showfun( data(1).mut, 'MUT1' );
%     showfun( data(2).wt, 'WT2' );
%     showfun( data(2).mut, 'MUT2' );
    
    % show summary
    show_exp( data(1) );
    show_exp( data(2) );
    
end

function y = normalise_quadruplet( y )

    m = min(y,[],1);
    M = max(y,[],1);
    assert( all( M-m > eps ), 'Flat data cannot be normalised.' );
    
    y = mean(m) + bsxfun( @times, bsxfun( @minus, y, m ), (mean(M)-mean(m))./(M-m) );
    
end

function [data,x,y] = read_data( filename )

    % open table
    raw  = readtable(filename);
    name = raw.Properties.VariableNames;
    
    % column names (first column is the dependent var)
    xname = name{1};
    yname = name(2:end);
    
    % parsed data
    x = raw.(xname);
    y = dk.cellfun( @(n) raw.(n), yname, false );
    y = horzcat(y{:});
    
    % reorder columns to group by quadruplet (Ax Ay Bx By)
    A = y(:,1:8);
    B = y(:,9:end);
    
    get_normalised_data = @(k) normalise_quadruplet([A(:,k),B(:,k)]);
    
    data(1).wt  = struct( 'x', x, 'y', get_normalised_data(1:2) );
    data(1).mut = struct( 'x', x, 'y', get_normalised_data(3:4) );
    
    data(2).wt  = struct( 'x', x, 'y', get_normalised_data(5:6) );
    data(2).mut = struct( 'x', x, 'y', get_normalised_data(7:8) );
    
end

function q = fit_quadruplet_all( q )

    f = cell(1,4);
    for i = 1:4
        f{i} = fit_logistic( q.x, q.y(:,i) );
    end
    q.fit = [f{:}];

end

function q = fit_quadruplet_avg( q )

    q.avg = struct('mean',mean(q.y,2),'std',std(q.y,[],2));
    q.fit = fit_logistic( q.x, q.avg.mean );

end

function idx = find_monotonic_increasing( y )

    connected   = bwconncomp(diff(y) >= 0);
    [~,largest] = max(cellfun( @numel, connected.PixelIdxList ));
    idx = connected.PixelIdxList{largest};

end

function out = fit_logistic( x, y )

    % find monotonic portion
    i  = find_monotonic_increasing(y);
    n  = numel(i);
    xi = x(i);
    yi = y(i);
    
    % initial guess for parameters
    lo = min(y);
    hi = max(y);
    mu = xi(fix(n/2));
    sg = find( yi >= (lo + 3*(hi-lo)/4), 1, 'first' );
    sg = mean(diff(x)) * (sg - n/2);
    
    lf = @(p,t) p(1) + (p(2)-p(1))./(1+exp( (p(3)-t)/p(4) ));
    [p,r] = lsqcurvefit( lf, [lo,hi,mu,sg], xi, yi );
    
    % make it easier to read the result
    out.lo = p(1);
    out.hi = p(2);
    out.mu = p(3);
    out.sg = p(4);
    
    out.res = r; % residual
    out.fun = @(t) lf(p,t);
    
end

function p = plot_avg( q, cbar, cfit )

    if nargin < 3, cfit = 'r-'; end
    if nargin < 2, cbar = 'k-'; end

    n = numel(q.x);
    w = mean(diff(q.x))/3;
    
    for i = 1:n
        l = q.x(i)-w/2;
        r = q.x(i)+w/2;
        b = q.avg.mean(i) - q.avg.std(i);
        t = q.avg.mean(i) + q.avg.std(i);
        plot( [l,r], t*[1,1], cbar ); hold on;
        plot( [l,r], b*[1,1], cbar );
        plot( q.x(i)*[1,1], [b,t], cbar );
    end
    p = plot( q.x, q.fit.fun(q.x), cfit, 'LineWidth', 2 ); hold off;
    xlabel('Temperature'); ylabel('Fluorescence');

end

function show_fit( q, name )

    figure('name',name);
    for i = 1:4
        subplot(2,2,i);
        scatter( q.x, q.y(:,i), 'ko' ); hold on;
        plot( q.x, q.fit(i).fun(q.x), 'r-' ); hold off;
        xlabel('Temperature'); ylabel('Fluorescence');
    end

end

function show_avg( q, name )
    figure('name',name); plot_avg(q);
end

function show_exp( data )

    assert( isscalar(data) && isstruct(data) );
    figure('name','Experiment summary');
    
    pw = plot_avg( data.wt, 'c-', 'b-' ); hold on;
    pm = plot_avg( data.mut, 'm-', 'r-' );

    legend([pw,pm],'Wild-type','Mutant');
    title(sprintf( 'WT:[mu=%.2f,sigma=%.2f], Mut:[mu=%.2f,sigma=%.2f]', ...
        data.wt.fit.mu, data.wt.fit.sg, data.mut.fit.mu, data.mut.fit.sg ));
    
end
