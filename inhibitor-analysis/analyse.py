#!/usr/bin/env python3

import os
import json
import argparse
import pandas as pd

from scipy import stats
from collections import namedtuple

# ------------------------------------------------------------------------

def rename_col(df,num,check,name):
    """
    Rename a DataFrame column by number.
    """
    old = df.columns[num]
    if old.strip().lower().startswith(check):
        df.rename( columns={ old: name }, inplace=True )
    else:
        raise KeyError('Failed check for column #%d: "%s" != "%s*"', num, old, check)

def convert_time(x):
    """
    Time-conversion
        min:sec (str)  =>  min (float)
    """
    m,s = x.strip().split(':')
    return float(m) + float(s)/60

# ------------------------------------------------------------------------

def parse_jobfile( file ):
    """
    Open JSON file and parse tasks to execute.
    Each task corresponds to a particular input file (field "input") + column number 
    (field "colnum"), and outputs a CSV file (field "output").
    """

    # load job data
    with open( file, 'r' ) as fp:
        job = json.load(fp)

    # validate job format
    for f in ['conditions','tasks']:
        if f not in job:
            raise KeyError('Missing field "%s" in JSON file.' % f)

    assert isinstance(job['conditions'],dict), 'Field "conditions" should be a dict'
    assert isinstance(job['tasks'],list), 'Field "tasks" should be a list'
    
    # iterate over tasks and try to execute each of them
    for tid, task in enumerate(job['tasks']):
        try:
            execute_task( task, job['conditions'] )
        except Exception as e:
            print(e)
            print("Could not execute task: %s" % str(task))

def execute_task( task, conditions ):
    """
    Open tabulated file for this task, parse specified columns (given column number and 
    conditions), format time-column in minutes, perform linear regression on fluorescence
    time-course, and save results for each condition in output file.

    Required task fields:
        colnum      column number across conditions
        input       tabulated file with fluorescence measurements

    Optional task fields:
        output      where the results should be saved (default: append '-analysis-colX.csv' to input)
        head        number of lines to skip at the top (default: 2)
        tail        number of lines to skip at the bottom (default: 4)
    """

    print( '\nProcessing file "%s"...' % task['input'] )

    # read CSV file
    df = pd.read_csv( task['input'], sep='\t', engine='python',
        skiprows=task.get('head',2), skipfooter=task.get('tail',4) )

    # rename first two columns
    rename_col( df, 0, 'time', 'Time' )
    rename_col( df, 1, 'temp', 'Temp' )

    # convert time to minutes
    df['Time'] = df['Time'].apply( convert_time )

    # convert column number to string
    colnum = str(task['colnum'])

    # linear regression to find slope
    LinFit = namedtuple( 'LinFit', 'condition slope intercept rsquared pvalue error' )
    results = []
    for letter, value in conditions.items():

        col = letter + colnum
        if any(pd.isna( df[col] )):
            raise TypeError('Could not read data in column %s of file "%s", is it the right column?' % (col,task['input']))

        s,i,r,p,e = stats.linregress( df['Time'], df[col] )
        results.append(LinFit( value, s, i, r*r, p, e ))

    # convert to DataFrame and save
    # suffix = '-analysis-col%s.csv' % colnum 
    output = os.path.splitext( task['input'] )[0] + '-analysis.csv'
    output = task.get( 'output', output )

    print( '\t=> "%s"' % output )
    pd.DataFrame(results).to_csv(output)

# ------------------------------------------------------------------------

if __name__ == "__main__":

    parser = argparse.ArgumentParser( prog='analyse',
        description='Process fluorescence results for inhibition experiments' )

    parser.add_argument( 'jobfile', help='Path to .json file' )
    args = parser.parse_args()
    parse_jobfile(args.jobfile)
