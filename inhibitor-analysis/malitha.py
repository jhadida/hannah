import pandas as pd
import numpy as np 
import string
from scipy import stats


def charRange(mini, maxi, step=1):
  if mini in string.ascii_uppercase:
    strg = string.ascii_uppercase
  else:
    strg = string.ascii_lowercase
  return (strg[i] for i in 
          range(string.ascii_lowercase.index(mini.lower()),
                string.ascii_lowercase.index(maxi.lower())+1,
                step))


def parsing(file, firstlet, number):
	"""Takes your file of interest, parses the columns you need and then writes a csv containing
		inhibitor conc, column that it took from, the slope, and r-squared"""

	#naming each of the columns
	columns = ['Time', 'Temp']
	for letter in charRange('A', 'P'):
		for value in range(1, 25):
			columns.append(str(letter + str(value)))
	
	#naming only the columns you want to keep
	usingcols = ['Time', 'Temp']
	for newlet in charRange(str(firstlet), 'P'):
			usingcols.append(str(newlet + str(number)))
	usingcols = usingcols[0:11]
	
	#importing data
	data = pd.read_csv(file, delimiter=',', header=None, skiprows=3, 
			skipfooter=4, names=columns, usecols=usingcols, engine='python')
	data = data.dropna(axis='columns')

	#changing time
	for lab, row in data.iterrows():
		minu = float((row['Time'])[0:2])
		sec = float((row['Time'])[3:5])
		times = (minu * 60) + sec
		data.loc[lab, 'Time (s)'] = times

	#finding slope
	slope_val = []
	for stri in usingcols[2:]:
		slope, intercept, r_value, p_value, std_err = stats.linregress(data['Time (s)'], data[stri])
		m = [slope * 60, r_value ** 2]
		slope_val.append(m)
	
	slope_val = np.transpose(slope_val)
	inhibitors = [0, 1000, 500, 100, 50, 10, 1, 0.1, 0.01]

	#outputting
	outputs = pd.DataFrame(slope_val, columns=inhibitors)
	outputs = outputs.T
	fileout = file[:-4]
	outputs.to_csv(path_or_buf=fileout+'_analysis.csv')


parsing("Inhibitor 1 III (test).csv", 'A', 3)
