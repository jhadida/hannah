#!/bin/bash

SOURCE="/vols/Scratch/jhadida/temp/Hannah/inhibitor-analysis.zip"
TARGET="data.zip"

scp "jhadida@jalapeno.fmrib.ox.ac.uk:$SOURCE" "$TARGET"
[ -f "$TARGET" ] && unzip "$TARGET"
